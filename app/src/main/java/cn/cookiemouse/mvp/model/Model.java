package cn.cookiemouse.mvp.model;

import android.util.Log;

import com.alibaba.fastjson.JSON;

import java.io.IOException;

import cn.cookiemouse.mvp.beans.TestBean;
import cn.cookiemouse.mvp.presenter.Presenter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Model {
    private static final String TAG = "Model";

    private Presenter mBasePresenter;

    public Model(Presenter presenter) {
        this.mBasePresenter = presenter;
    }

    /**
     * 加载数据
     */
    public void loadData() {
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("http://www.weather.com.cn/data/sk/101010100.html")
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i(TAG, "onFailure: ");
                mBasePresenter.loadDataFailure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String str = response.body().string();
                Log.i(TAG, "onResponse: -->" + str);
                TestBean testBean = JSON.parseObject(str, TestBean.class);
                mBasePresenter.loadData(testBean);
            }
        });
    }
}
