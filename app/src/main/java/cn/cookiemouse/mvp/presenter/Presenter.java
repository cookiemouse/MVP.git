package cn.cookiemouse.mvp.presenter;

import cn.cookiemouse.mvp.beans.TestBean;
import cn.cookiemouse.mvp.model.Model;
import cn.cookiemouse.mvp.view.IView;

public class Presenter implements IPresenter {

    private Model mModel;
    private IView mIView;

    public Presenter(IView iView) {
        this.mIView = iView;
        mModel = new Model(this);
    }

    @Override
    public void loadData(TestBean bean) {
        mIView.dismissLoading();
        mIView.loadData(bean);
    }

    @Override
    public void loadDataFailure() {
        mIView.dismissLoading();
    }

    public void loadData() {
        mIView.showLoading();
        mModel.loadData();
    }
}
