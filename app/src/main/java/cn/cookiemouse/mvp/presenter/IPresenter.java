package cn.cookiemouse.mvp.presenter;

import cn.cookiemouse.mvp.beans.TestBean;

public interface IPresenter {
    /**
     * 加载成功回传数据
     *
     * @param bean
     */
    void loadData(TestBean bean);

    /**
     * 加载失败
     */
    void loadDataFailure();
}
