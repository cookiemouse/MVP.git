package cn.cookiemouse.mvp.view;

import cn.cookiemouse.mvp.beans.TestBean;

public interface IView {
    void loadData(TestBean bean);

    void showLoading();

    void dismissLoading();
}
