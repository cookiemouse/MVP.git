package cn.cookiemouse.mvp.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cn.cookiemouse.dialogutils.LoadingDialog;
import cn.cookiemouse.mvp.R;
import cn.cookiemouse.mvp.beans.TestBean;
import cn.cookiemouse.mvp.presenter.Presenter;
import cn.cookiemouse.mvp.view.IView;

public class MainActivity extends AppCompatActivity implements IView, View.OnClickListener {

    private static final String TAG = "MainActivity";

    private Button mButtonRequest;
    private TextView mTextViewResult;

    private Presenter mPresenter;

    private LoadingDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_activity_main: {
                mPresenter.loadData();
                break;
            }
            default: {
                Log.i(TAG, "onClick: default");
            }
        }
    }

    @Override
    public void loadData(TestBean bean) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("城市：");
        stringBuilder.append(bean.getWeatherinfo().getCity());
        stringBuilder.append("\n温度：");
        stringBuilder.append(bean.getWeatherinfo().getTemp());
        stringBuilder.append("\n风向：");
        stringBuilder.append(bean.getWeatherinfo().getWD());
        stringBuilder.append("\n风力大小：");
        stringBuilder.append(bean.getWeatherinfo().getWS());
        stringBuilder.append("\n湿度：");
        stringBuilder.append(bean.getWeatherinfo().getSD());
        stringBuilder.append("\n时间：");
        stringBuilder.append(bean.getWeatherinfo().getTime());
        Runnable runnable = () -> mTextViewResult.setText(stringBuilder);
        runOnUiThread(runnable);
    }

    @Override
    public void showLoading() {
        dismissLoading();
        mLoadingDialog = LoadingDialog.with(this).show();
    }

    @Override
    public void dismissLoading() {
        if (null != mLoadingDialog) {
            mLoadingDialog.dismiss();
        }
    }

    private void init() {
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mButtonRequest = findViewById(R.id.btn_activity_main);
        mTextViewResult = findViewById(R.id.tv_activity_main);
    }

    private void initData() {
        mPresenter = new Presenter(this);
    }

    private void initEvent() {
        mButtonRequest.setOnClickListener(this);
    }
}
